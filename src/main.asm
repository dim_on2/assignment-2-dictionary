%include  "src/words.inc"
%include  "src/lib.inc"

%define   BUFFER_SIZE 256
%define   INCORRECT_TERMINATION_ROUTINE 0
%define   POINTER_SIZE 8
%define   POINTER_TO_LAST_DICT_WORD NEXT

section .bss
  buffer: resb BUFFER_SIZE

section .rodata
  hello_string: db "What do you search?", 10, 0
  error_string: db "Word greater than buffer.", 10, 0
  no_such:  db "No such a pointer to a word of the dictionary as given", 10, 0
extern  find_word

global _start
section .text
_start:
  mov   rdi, hello_string
  call  print_string
  mov   rdi, buffer
  mov   rsi, BUFFER_SIZE
  call  read_word
  cmp   rdx, INCORRECT_TERMINATION_ROUTINE
  je    .error_buffer
  mov   rsi, POINTER_TO_LAST_DICT_WORD
  mov   rdi, buffer
  call  find_word
  cmp   rax, INCORRECT_TERMINATION_ROUTINE
  je    .not_key
  add   rax, POINTER_SIZE
  mov   rdi, rax
  push  rax
  call  string_length
  pop   rdi
  add   rdi, rax
  inc   rdi
  call  print_string
  mov   rdi, 0
  jmp   .exit
  .not_key:
    mov   rdi, no_such
    jmp   .error_exit
  .error_buffer:
    mov   rdi, error_string
  .error_exit:
    call  print_error_string
    mov   rdi, 1
  .exit:
    call  exit