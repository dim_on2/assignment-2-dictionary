%include  "src/lib.inc"
%define   END_POINTER 0
%define   POINTER_SIZE 8
global  find_word

section .text
; rdi - poiter to null-terminated key string
; rsi - pointer to the last word in the dictionary
; If the record is not found, it returns zero; otherwise it returns record address
find_word:
  .loop:
    push  rsi
    push  rdi
    lea   rsi, [rsi + POINTER_SIZE] ; address dictionary key string  
    call  string_equals
    pop   rdi
    pop   rsi
    cmp   rax, 1
    je    .success
    cmp   qword [rsi], END_POINTER ; compare pointer to the word with end pointer of the dictionary
    jz    .error
    mov   rsi, [rsi]
    jmp   .loop
  .error:
    xor   rax, rax
    ret
  .success:
    mov   rax, rsi
    ret