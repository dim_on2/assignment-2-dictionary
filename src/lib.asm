%define    QUIT    60
%define    READ    0
%define    WRITE   1
%define    ERROR   2
%define    ENDING_STRING   0xA

global exit
global string_length
global print_string
global print_error_string
global print_newline
global print_char
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy

section .text

; Принимает код возврата и завершает текущий процесс
exit: 
    ;rdi - код возврата
    mov rax, QUIT
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    ; rdi - указатель на нуль-терминированную строку
    ; rax - возвращает её длину
    mov rax, 0
    .loop:
        cmp byte [rdi + rax], 0
        je .exit
        inc rax
        jmp .loop
    .exit:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    ; rdi - указатель на нуль-терминированную строку
    push rdi
    call string_length
    mov rdx, rax ; character number
    mov rax, WRITE
    pop rsi
    mov rdi, 1 ; stdout
    syscall
    ret

print_error_string:
    ; rdi - указатель на нуль-терминированную строку
    push rdi
    call string_length
    mov rdx, rax ; character number
    mov rax, WRITE
    pop rsi
    mov rdi, ERROR ; stdout
    syscall
    ret


; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, ENDING_STRING

; Принимает код символа и выводит его в stdout
print_char:
    ; rdi - код символа
    push di
    mov     rsi, rsp
    mov     rdx, 1 ; character number
    mov     rax, WRITE
    mov     rdi, 1 ; stdout
    syscall
    pop di
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    ;rdi - беззнаковое 8-байтовое число
    mov     r8, rsp             ; сохранить начальный указатель стека
    dec     rsp                 ; сделать строку
    mov     byte [rsp], 0x0     ;       остатков нуль-терминированной
    mov     rax, rdi            ; положить число в rax
    mov     rdi, 0xA            ; делитель
 .loop:
    dec     rsp
    xor     rdx, rdx            ; очищаем rdx - в нем остаток от деления
    div     rdi                 ; при аргументе r/m64 - RDX:RAX = 0:<наше число> и после деления получим, что частное в rax, а остаток в rdx
    add     dl, 0x30            ; получаем ASCII код
    mov     [rsp], dl           ; сохраняем 
    cmp     rax, 0              ; если делимое 
    jne     .loop               ;       не ноль - продолжаем делить
    mov     rdi, rsp            ; указываем начало строки
    push    r8                  ; сохраняем регистр с начальным указателем стека, так как он - caller-saved
    call    print_string        ; выводим строку
    pop     r8                  
    mov     rsp, r8             ; вернуть стек в исходное состояние
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    ; rdi - знаковое 8-байтовое число
    test    rdi, rdi        ; проверяем,  
    jns     print_uint      ;       что число положительное и выводим его
    push    rdi
    mov     rdi, '-'        ; иначе:
    call    print_char      ;       выводим минус
    pop     rdi
    neg     rdi             ;       выводим инвертированное число
    call    print_uint
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    ; rdi - указатель на первую строку
    ; rsi - указатель на вторую строку
    mov    rcx, -1
    .loop:
        inc     rcx
        mov     al, [rdi+rcx]
        mov     r8b, [rsi+rcx]
        cmp     al, r8b
        jne     .fail
        test    al, -1
        jne     .loop
        mov     rax, 1
        jmp     .exit
    .fail:
        xor     rax, rax
    .exit:
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    ; return rax - один символ из stdin
    xor     rax, rax        ; номер системного вызова для чтения
    mov     rdi, READ       ; дескриптор stdin
    lea     rsi, [rsp - 1]  ; ссылка на буфер, то есть один байт выше стека
    mov     byte [rsi], 0x0 ; защита от мусорных значений для конца потока
    mov     rdx, 1          ; кол-во символов для чтения
    syscall
    mov     al, byte [rsi]  ; сохранение символа в rax
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    ; rdi - адрес начала буфера
    ; rsi - размер буфера
    xor     r8, r8
    _A:
        push    rdi         ;сохраняем адрес начала буфера
        push    rsi         ;сохраняем размер буфера
        call    read_char   ;читаем символ
        pop     rsi         ;достаем размер буфера
        pop     rdi         ;достаем адрес начала буфера
        cmp     al, 0x20    ; сравниваем символ с пробелом
        je      _A          ;если да - читаем снова
        cmp     al, 0x9     ; сравниваем символ с tab
        je      _A          ; если да - читаем снова
        cmp     al, 0xA     ; сравниваем символ с переводом строки
        je      _A          ; если да - читаем снова
        cmp     al, 0x0     ; конец потока
        je      _D          ; если да - выход, слова нет
    _B:
        inc     r8          ; если здесь, то прочитали некоторый символ - счетчик + 1
        dec     rsi         ; уменьшаем оставшееся кол-во ячеек доступных для записи
        cmp     rsi, 0      ; если таковых уже нет, выходим
        je      _D          
        mov     byte [rdi + r8 - 1], al ; сохранение символа в массив
        push    rdi         ;сохраняем адрес начала буфера
        push    rsi         ;сохраняем размер буфера
        call    read_char   ;читаем символ
        pop     rsi         ;достаем размер буфера
        pop     rdi         ;достаем адрес начала буфера
        cmp     al, 0x20    ; сравниваем с пробелом
        je      _C          ;если да - конец слова
        cmp     al, 0x9     ; сравниваем с tab
        je      _C          ; если да - конец слова
        cmp     al, 0xA     ; сравниваем с переводом строки
        je      _C          ; если да - конец слова
        cmp     al, 0x0     ; конец потока
        je      _C          ; если да - конец слова
        jmp     _B          ; иначе снова обрабатываем символ
    _C:
        mov     byte [rdi + r8], 0x0    ; нуль-терминатор для строки
        mov     rax, rdi    ;   указатель на буфер
        mov     rdx, r8     ;   кол-во символов
        ret
    _D:
        xor     rax, rax    ;   ошибка
        xor     rdx, rdx
        ret
 
; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    ; rdi - указатель на строку
    xor     r8, r8         ; кол-во байт в числе
    xor     rax, rax        
    xor     rdx, rdx       ; нужно для умножения(появление здесь числа означает превышение 2^64, то есть число не помещается в rax)
    mov     r10, 0xa       ; 10 - то на что умножаем
    .loop:
        xor     r9, r9  
        mov     r9b, [rdi]  ; читаем символ
        inc     rdi         ; устанавливаем указатель на след. байт строки 
        cmp     r9b, '0'    ; не число - успешно
        jb      .success
        cmp     r9b, '9'    ; не число - успешно
        ja      .success
        mul     r10         ; умножаем на 10
        cmp     rdx, 0      ; проверка, что число поле умножения помещается в rax
        jne     .error
        sub     r9b, 0x30   ; делаем символ числом
        add     rax, r9     ; добавляем к rax
        inc     r8          ; увеличиваем кол-во символов в числе
        jmp     .loop       ; снова читаем символ...
    .success:
        mov     rdx, r8     ; устанавливаем кол-во символов в rdx, в rax уже лежит число
        ret
    .error:
        xor     rdx, rdx
        xor     rax, rax
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    ; rdi - указатель на строку
    xor     r9, r9  
    mov     r9b, [rdi]  ; читаем символ
    cmp     r9b, '-'
    je      .negative
    call    parse_uint
    ret
    .negative:
        inc     rdi
        call    parse_uint
        cmp     rdx, 0
        je      .error
        add     rdx, 1
        neg     rax
        ret
    .error:
        xor     rax, rax
        xor     rdx, rdx
        ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    ; rdi - указатель на строку
    ; rsi - указатель на буфер
    ; rdx - длину буфера
    xor     r8, r8
    xor     rax, rax
    .loop:
        cmp     r8, rdx
        ja      .error
        mov     al, [rdi + r8] 
        mov     byte [rsi + r8], al
        cmp     al, 0x0
        je      .quit
        inc     r8
        jmp     .loop
    .error:
        xor     rax, rax
        ret
    .quit:
        mov     rax, r8
        ret