%define NEXT 0

%macro colon 2
  %ifid %2
    %2: dq NEXT ; value_pointer
    %define NEXT %2  
  %else
    %error "Pointer to value isn't right"
  %endif

  %ifstr %1
    db %1, 0 ; dictionary key
  %else
    %error "Dictionary key must be string"
  %endif
  
%endmacro