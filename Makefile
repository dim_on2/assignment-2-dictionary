ASM=nasm
LD=ld
ASMFLAGS=-f elf64 -g
SRC_DIR=src
EXECUTABLE=main
OBJ_DIR=obj

SOURCES := $(wildcard $(SRC_DIR)/*.asm)
OBJECTS := $(SOURCES:$(SRC_DIR)/%.asm=$(OBJ_DIR)/%.o)

all: $(EXECUTABLE)

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.asm 
	@mkdir -p $(OBJ_DIR)
	$(ASM) $< $(ASMFLAGS) -o $@

$(EXECUTABLE): $(OBJECTS)
	$(LD) $^ -o $@

$(SRC_DIR)/$(EXECUTABLE).asm: $(SRC_DIR)/*.inc
	touch $@

$(SRC_DIR)/dict.asm: $(SRC_DIR)/lib.inc
	touch $@

execute:
	./$(EXECUTABLE)

clean:
	rm -f $(OBJ_DIR)/*.o $(EXECUTABLE)

.PHONY: execute clean all